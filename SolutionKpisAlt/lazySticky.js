import React from "react";

const LazySticky = ({ children, ...props }) => {

  const Sticky = useSticky();
  return (
    <Sticky {...props}>
      {children}
    </Sticky>
  )
}

/**
 * Hook : load slider function
 * @param {Boolean} shouldDisplay 
 */
function useSticky() {
  const [sticky, setSticky] = React.useState(() => ShadowComp);

  React.useEffect(() => {
    import(`react-stickynode`)
      .then(({ default: Sticky }) => setSticky(() => Sticky));
  }, []);

  return sticky;
}

/**
 * Shadow component
 */
const ShadowComp = ({ children, className }) => {
  return (
    <div className={className}>
      {
        typeof children === "function" ?
          children({ status: 0 }) :
          children
      }
    </div>
  )
}

export default LazySticky;