// LIVE HERE : https://www.swile.co/en/solutions/mobility

import React from 'react';
import { RichText } from 'prismic-reactjs';

// Utils
import PrismicImage from "./prismicImg";
import { PrismicLink } from "/prismic-configuration";
import StickyScreen, {ScrollContext} from "./stickyScreen";
import useBreakpoint from "./useBreakpoint";

// CSS
import style from "./index.module.scss";

const ACTIVE_STATE = [
  [-99, 20],
  [20, 40],
  [40, 100]
];

const LARGE_BREAKPOINT = 1025;

const SolutionKpisAlt = ({ slice }) => {
  const primary = slice.primary || {};
  const items = slice.items || [];
  const isNotLarge = useBreakpoint(LARGE_BREAKPOINT);

  return (
    <>
      <section className={`${style.Scroll} BgBlack White`}>
        <StickyScreen
          className={`${style.Screen} ${style.Component} `}
          totalScreens={2}
          top={56}
          enabled={!isNotLarge}
          resetState={false}
        >
          <Elements items={items} primary={primary} />

        </StickyScreen>
      </section>
      <div id="Boundary" className={`Relative`}/>
    </>
  );
}

SolutionKpisAlt.displayName = SolutionKpisAlt

export default SolutionKpisAlt;

const Elements = ({primary, items}) => {
  const state = React.useContext(ScrollContext);

  const image = primary.image || {};
  const dimensions = image.dimensions || {};

  return (
    <div className={`wrapper`}>

      <div className={`Center mb5 mbmd10 ${style.TextGradient}`}>
        <RichText render={primary.title} />
      </div>

      <div className={`Relative`}>
        {
          image && image.url &&
          <span
            aria-hidden={true}
            className={`${style.Placeholder} mb4`}
            style={
              {
                paddingBottom: `${100 * dimensions.height / dimensions.width}%`,
              }
            }
          />
        }

        <div className={`grid mbmd6`}>
          <div className={`grid__item lg7`}>
            <div className={style.Kpis}>
              {
                items.slice(0,3).map(
                  (item, key) => <Kpi 
                    key={`kpi-${key}`} 
                    active={state >= ACTIVE_STATE[key][0]  && state < ACTIVE_STATE[key][1] }
                    {...item} 
                  />
                )
              }
            </div>
          </div>
          <div className={`grid__item lg5`}>
            <div className={style.Image}>
              {
                image && image.url &&
                <PrismicImage
                  {...image}
                  alt={RichText.asText(primary.title)}
                  sizes={`95vw, (min-width: 769px) 40vw, (min-width: 1200px) 500px`}
                />
              }
            </div>
            {
              primary.footnote &&
              <p className={`${style.Footnote} p1`}>
                {RichText.asText(primary.footnote)}
              </p>
            }
          </div>
        </div>

        <div className={`${style.Buttons} Center`}>
          <div className={`mb2`}>
            {
              primary.buttonLight &&
              <PrismicLink
                {...primary.linkLight}
                className={`Btn Btn--Big Btn--Grey`}
              >
                {primary.buttonLight}
              </PrismicLink>
            }
          </div>
          <div>
            {
              primary.buttonDark &&
              <PrismicLink
                {...primary.linkDark}
                className={`Btn Btn--Big Btn--Black`}
              >
                {primary.buttonDark}
              </PrismicLink>
            }
          </div>
        </div>
      </div>
    </div>
  )
}


const Kpi = ({ title, kpi, footnote, active }) => {
  return (
    <article className={`${style.Kpi}${active ? ` ${style.Active}` : ""} mb5`}>
      <div className={`mb2 mbmd0 Center`}>
        <span className={`Swile ${style.Number}`}>
          {kpi}
        </span>
      </div>
      <div>
        <h3 className={`t6 Medium mb1 ${style.Kpi__Title}`}>
          {RichText.asText(title)}
        </h3>
        {
          footnote &&
          <p className={`p1 Fade--60`}>
            {RichText.asText(footnote)}
          </p>
        }
      </div>
    </article>
  )
}