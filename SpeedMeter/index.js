// LIVE HERE : https://www.swile.co/en/solutions/gift

import React from 'react';
import { RichText } from 'prismic-reactjs';

// Utils
import useInViewport from "./useInViewport";
import useJsonAnimation from "./useJsonAnimantion";
import {PrismicLink} from "/prismic-configuration";

// Css
import style from "./index.module.scss";

const REPLACE_TXT = "#{number}"

const SolutionSpeedMeter = ({ slice }) => {
  const [number, setNumber] = React.useState(0);
  const [container, isInViewport] = useInViewport(null, {
    setViewport: (watcher) => {
      return watcher.isFullyInViewport;
    }
  });

  const primary = slice.primary || {};
  const items = slice.items || [];

  const title = RichText.asText(primary.title) || REPLACE_TXT;
  const [part1, part2] = title.split(REPLACE_TXT);

  const [animRef, animation] = useJsonAnimation(
    "/static/json/Titre-cadeau-Jauge.json",
    {
      loop: false,
      autoplay: false,
    },
    () => {return;}
  );

  /**
   * Increment number from 0 to 100%
   */
  const incrementNumber = React.useCallback(
    () => {
      // Wait for animation to be loaded to fire 
      let loop = setInterval(
        () => {
          const anim = animation.current;
          const totalTime = 1600;
          // const totalTime = 1000 * anim.totalFrames / anim.frameRate;

          let currentTime;
          if (anim && anim.isLoaded) {
            anim.goToAndPlay(0);
            (function fn(timestamp) {
              if (!currentTime) currentTime = timestamp;
              const progress = timestamp ? 
                100 * (timestamp - currentTime) / totalTime :
                0;
              setNumber(parseInt(progress));

              if (progress < 100) {
                window.requestAnimationFrame(fn);
              }
            })();
            clearInterval(loop);
          }
        }, 
        100
      );
      return () => clearInterval(loop);
    },
    [setNumber]
  );


  React.useEffect(
    () => {
      if (isInViewport) incrementNumber();
    },
    [
      isInViewport,
      incrementNumber
    ]
  )

  return (
    <section 
      className={`${style.Component} BgWhite Black`}
    >
      <div className={`wrapper`}>

        <div 
          className={`${style.Image} mb4`}
          ref={container}
        >
          <div className={`${style.Animation}`}>
            <div ref={animRef}/>
          </div>
        </div>

        <h2 className={`Swile t2 Center mb2 ${style.TextGradientPink}`}>
          {part1} <em>{`${number}%`}</em><br/>{part2}
        </h2>
        <p className={`Center t6 Medium mb4`}>
          {RichText.asText(primary.description)}
        </p>

        <div className={`grid-flex--justify mb7`}>
          {
            items.map(
              (item, key) => <Item {...item} key={`item-${key}`} />
            )
          }
        </div>

        <div className={`Center`}>
          <PrismicLink
            className={`Btn Btn--Black Btn--Big`}
            {...primary.link}
          >
            {primary.button}
          </PrismicLink>
        </div>

      </div>
    </section>
  );
}

SolutionSpeedMeter.displayName = SolutionSpeedMeter

export default SolutionSpeedMeter;


const Item = ({title}) => {
  return (
    <div 
      className={`grid-flex__item md3 lg3 mb4`}
      data-aos={`fade-in`}
    >
      <span className={`${style.Img} mb3`} aria-hidden={true} />
      <h3 className={`p0 Medium Center`}>
        {RichText.asText(title)}
      </h3>
    </div>
  )
}