/// LIVE HERE: https://www.supermood.com/en-gb/about

import React from 'react';
import { RichText } from 'prismic-reactjs';

// Utils
import r from "./renderer";
import PrismicImage from "./prismicImg";
import { linkResolver } from "/prismic-configuration";
import { customRenderer } from "./txt";

// Css
import style from "./index.module.scss";

const AboutSection = r()
  .withRender(
    ({ title, subtitle, description, image = {}, bgColor, imagePosition }) => {
      bgColor = bgColor || "Black";
      const reverse = !(imagePosition === null || imagePosition === true);
      const grid = reverse ? {
        left: "",
        right: "lmd1 llg1"
      } : {
        left: "hmd1 hlg1",
        right: "hmd2 hlg2"
      }

      const dimensions = image.dimensions || {};
      const modifiedTitle = customRenderer(title);

      return (
        <section className={`${style.Component} oHidden`} data-bg={`Bg${bgColor}`}>
          <div className={`wrapper`}>
            <div className={`grid-flex--center${reverse ? " grid-flex--reverse-md" : ""}`}>
              <div className={`grid-flex__item md6 lg6 ${grid.left}`}>
                <span className={`Block t7 Criteria FontBlack mb3`}>
                  {RichText.asText(subtitle)}
                </span>
                <h2
                  className={`FontBlack Criteria t2 mb3 ${style.Title}`}
                  dangerouslySetInnerHTML={
                    {
                      __html: modifiedTitle
                    }
                  }
                />
                <div className={`${style.Content} mb3 mbmd0`}>
                  <RichText render={description} linkResolver={linkResolver} />
                </div>
              </div>
              <div className={`grid-flex__item md4 lg4 ${grid.right}`}>
                <div className={style.Image}>
                  <PrismicImage
                    {...image}
                    alt={RichText.asText(subtitle)}
                    sizes={`100vw, (min-width: 769px) 35vw, (min-width: 1200px) 400px`}
                    layout={`intrinsic`}
                    dimensions={
                      {
                        width: dimensions.width / 2,
                        height: dimensions.height / 2,
                      }
                    }
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
      )
    }
  )

AboutSection.displayName = AboutSection

export default AboutSection;
