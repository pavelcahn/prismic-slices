import React from "react";
import Sticky from "./lazySticky";

export const ScrollContext = React.createContext(0);

/**
 * Give value between 0 and 1
 * @param {number} value 
 */
export const capOpacity = (value) => capValue(value, [0, 1]);

/**
 * @param {number} value Value to be caped
 * @param {number[]} minMax Tuple min,max value 
 * @returns 
 */
export const capValue = (value, [min, max]) => Math.max(min, Math.min(max, value));

/**
 * @param {number[]} values Values to be caped
 * @param {number[]} minMax Tuple min,max value 
 * @returns 
 */
export const capValues = (values, [min, max]) => Math.max(min, Math.min(max, ...values));

/**
 * Truncate number
 * @param {number} n
 * @return {number}
 */
export const truncNumber = (n) => parseInt(100 * n) / 100;

/**
 * Default element
 * @param {HTMLElement} el 
 * @returns 
 */
const defaultElement = (el) => el.firstChild;


/**
 * When param is set to true, start listening on scroll event 
 * and scroll completion based on window height
 * @param {Boolean} isFixed Scroll listener indicator
 * @param {number} totalScreens Number of screens to scroll
 * @param {function} getElement Callback to get element watched based on container
 * @returns {Array[Number, React.ref]} Percentage and ref to attach to watched component
 */
const useAnimationProgress = (
  isFixed,
  {
    totalScreens = 2,
    resetState = true,
    getElement = defaultElement,
    monitorBefore = false
  }
) => {
  const container = React.useRef();
  const [state, setState] = React.useState(0);

  /**
   * Calculate state based on element position in scroll
   */
  const getNextState = React.useCallback(
    () => {
      if (container.current) {
        const animationLength = totalScreens * window.innerHeight;
        const element = getElement(container.current);
        const rect = element.getBoundingClientRect();
        const progress = truncNumber(-(rect.y / animationLength) * 100);
        return Math.min(progress, 100);
      }
      return 0;
    },
    [totalScreens]
  );

  /**
   * Calculate animation state (percentage) based on scroll
   */
  const updateScrollProgress = React.useCallback(() => {
    window.requestAnimationFrame(
      () => {
        const nextState = getNextState();
        setState(nextState);
      }
    )
  }, [setState]);


  React.useEffect(() => {
    // Calculate scroll progress
    if (isFixed || monitorBefore) window.addEventListener("scroll", updateScrollProgress);

    // Reset state when not in sticky state
    if (!isFixed && resetState) setState(0);
    return () => {
      // Remove listener
      window.removeEventListener("scroll", updateScrollProgress);
    }
  }, [
    setState,
    isFixed,
    updateScrollProgress,
    monitorBefore
  ]);

  return [state, container];
}

/**
 * Sticy state
 */
const useStickyState = () => {
  const [isFixed, setFixed] = React.useState(false);

  /**
   * Update component state
   */
  const handleStateChange = React.useCallback(
    ({ status }) => setFixed(status === 2),
    [setFixed]
  );

  return [isFixed, handleStateChange];
}

/**
 * Zoom animation on scroll
 * @param {function} handleChange Callback fired when state changed. 
 *                   Takes state as argument
 * @param {boolean} resetState Sould state return to 0 when container is not sticky 
 * @param {number} totalScreens Number of screen the scroll progress is based on
 * @param {string|number} boundary Bottom bourdary, tells when to release sitckyness
 */
const StickyScreen = (
  {
    children,
    handleChange,
    resetState = true,
    totalScreens = 2,
    className = ``,
    style = () => ({}),
    boundary = `#Boundary`,
    top = 0,
    enabled = true,
    monitorBefore = false
  }
) => {
  const [isFixed, handleStateChange] = useStickyState();

  const [state, container] = useAnimationProgress(isFixed, {
    totalScreens,
    resetState,
    monitorBefore
  });

  const containerStyle = React.useMemo(
    () => style(state),
    [style, state]
  );

  // Access state from outside the function
  React.useEffect(
    () => {
      handleChange && handleChange(state)
    },
    [state, handleChange]
  );

  return (
    <ScrollContext.Provider value={state}>
      <div
        ref={container}
        className={className}
        style={containerStyle}
      >
        <Sticky
          top={top}
          enabled={enabled}
          onStateChange={handleStateChange}
          bottomBoundary={boundary}
        >
          {children}
        </Sticky>
      </div>
    </ScrollContext.Provider>
  )
}

export default StickyScreen;