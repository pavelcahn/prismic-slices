// LIVE HERE : https://www.swile.co/en/solutions/insights

import React from 'react';
import { RichText } from 'prismic-reactjs';

// Utils
import PrismicImage from "./prismicImg";
import { PrismicLink } from "/prismic-configuration";
import useInViewport from "./useInViewport";

// Css
import style from "./index.module.scss";

const CardThird = ({ title, content, image }) => {
  return (
    <div className={`grid-flex__item md4 lg4 mb2 mbmd0`}>
      <article className={`${style.Card} ${style.CardThird}`}>
        <div className={`${style.Card__Img}`}>
          <PrismicImage
            {...image}
            alt={RichText.asText(title)}
            sizes={`95vw, (min-width: 769px) 33vw, (min-width: 1200px) 400px`}
            layout={`fill`}
            objectFit={`cover`}
          />
        </div>
        <div className={style.Card__Content}>
          <h4 className={`t6 mb1 Center`}>
            {RichText.asText(title)}
          </h4>
          <p className={`p0 Center`}>
            {RichText.asText(content)}
          </p>
        </div>
      </article>
    </div>
  )
}

const CardHalf = ({ title, content, image }) => {
  return (
    <div className={`grid-flex__item md5 lg5 mb2 mbmd0`}>
      <article className={`${style.Card} ${style.CardHalf}`}>
        <div className={style.Card__Img}>
          <PrismicImage
            {...image}
            alt={RichText.asText(title)}
            sizes={`95vw, (min-width: 769px) 40vw, (min-width: 1200px) 500px`}
            layout={`fill`}
            objectFit={`cover`}
          />
        </div>
        <div className={style.Card__Content}>
          <h4 className={`t6 Medium mb1 Center`}>
            {RichText.asText(title)}
          </h4>
          <p className={`p0 Center`}>
            {RichText.asText(content)}
          </p>
        </div>
      </article>
    </div>
  )
}

const rows = [
  {
    margin: `mb8 mbmd0`,
    indexes: [0, 3],
    Card: CardThird
  },
  {
    margin: `mb8 mbmd0`,
    indexes: [3, 6],
    Card: CardThird,
    grid: ``
  },
  {
    margin: `mb6 mbmd0`,
    indexes: [6,8],
    Card: CardHalf
  }
];

const SolutionThreeSteps = ({ slice }) => {
  const [container, isInViewport] = useInViewport();
  const previousActiveKey = React.useRef(-1);
  const [active, setActive] = React.useState(0);
  const [currentProgress, setProgress] = React.useState(0);
  const [paused, setPaused] = React.useState(false);
  const wasPaused = React.useRef(false);

  React.useEffect(
    () => () => { wasPaused.current = paused;},
    [paused]
  );


  React.useEffect(
    () => {
      if (isInViewport) {
        let time;
        let animation;
        const fn = (timestamp) => {
          if (!time) time = timestamp;
          const progress = Math.min((timestamp - time) / 5000, 1);
          setProgress(progress);

          if (timestamp < time + 5000) {
            animation = window.requestAnimationFrame(fn);
          } else {
            if (!paused) setActive((current) => (current + 1) % 3)
          }
        }

        if (!paused) {
          animation = window.requestAnimationFrame(fn);
          return () => window.cancelAnimationFrame(animation);
        }

      }
    },
    [
      paused,
      active,
      isInViewport,
      setActive
    ]
  );

  const handleClick = React.useCallback(
    (index) => (evt) => {
      evt.preventDefault();
      setActive(index);
    },
    [setActive]
  );

  React.useEffect(
    () => {
      previousActiveKey.current = active;
    },
    [active]
  );

  const primary = slice.primary || {};
  const items = slice.items || [];

  return (
    <section 
      className={`${style.Component} BgBlack White`}
      data-active={active}
    >
      <div className={`wrapper`}>
        <div className={`t2 Center mb8 mbmd4 ${style.TextGradient} ${style.TextGradientAnim}`}>
          <RichText render={primary.title} />
        </div>

        <div className={`${style.Nav} ${style.desktop} mb4`}>
          <span aria-hidden={true} />
          <span 
            aria-hidden={true}
            style={
              {
                transform: `scaleX(${ ((active % 3) / 3) + (currentProgress / 3)})`
              }
            }
          />
          {
            new Array(3).fill().map(
              (_,key) => (
                <button
                  key={`button-${key}`}
                  onClick={handleClick(key)}
                  onMouseEnter={() => setPaused(true)}
                  onMouseLeave={() => setPaused(false)}
                  className={`White${active === key ? ` ${style.Active}` : ""}`}
                >
                  <span className={style.Number}>{key+1}</span>
                  <h3 className={`t3 Swile Center mb2`}>
                    {primary[`step0${key+1}`]}
                  </h3>
                </button>
              )
            )
          }
        </div>
        <div 
          ref={container}
          className={`mbmd6`}
        >
          <Cards 
            rows={rows} 
            active={active} 
            previousActiveKey={previousActiveKey} 
            primary={primary}
            items={items} 
          />
        </div>

        <div className={`Center`}>
          <PrismicLink
            {...primary.link}
            className={`Btn Btn--Big Btn--White`}
          >
            {primary.button}
          </PrismicLink>
        </div>
      </div>
    </section>
  );
}

SolutionThreeSteps.displayName = SolutionThreeSteps

export default SolutionThreeSteps;

const Cards = ({ rows, active, previousActiveKey, primary, items}) => {
  return rows.map(
    ({ Card, margin, indexes }, key) => {
      let className = `${margin} ${style.Row}`;
      if (key === active) className += ` ${style.Active}`;
      if (key === previousActiveKey.current) className += ` ${style.PrevActive}`
      return (
        <div
          className={className}
          key={`row-${key}`}
        >
          <div className={style.mobile}>
            <span className={style.Number}>
              {key + 1}
            </span>
            <h3 className={`t3 Swile Center mb2`}>
              {primary[`step0${key + 1}`]}
            </h3>
          </div>
          <div className={`grid-flex grid-flex--justify`}>
            {
              items.slice(indexes[0], indexes[1]).map(
                (item, key) => <Card
                  {...item}
                  key={`card-${key}`}
                />
              )
            }
          </div>
        </div>
      )
    }
  )
}