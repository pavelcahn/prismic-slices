import React from "react";

const renderer = function () {
  return {
    withFetch: function (fn) {
      this.fetch = function () {
        const [fetched, setFetchedData] = React.useState(0);
        React.useEffect(() => fn(setFetchedData), [fn]);
        return fetched;
      }
      return this;
    },
    withRender: function (Component) {
      return ({ slice = {} }) => {
        const fetched = typeof this.fetch === "function" ? this.fetch() : null;
        const primary = slice.primary || {};
        const items = slice.items || [];
        return <Component {...primary} fetched={fetched} items={items} />
      }
    }
  }
}

export default renderer;