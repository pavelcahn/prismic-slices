/**
 * Get words from sentence
 * @param {string} str 
 * @return {string[]}
 */
export const getWords = (str) => str.split(" ");

/**
 * Place a span on the nth word
 * @params {string[]} words
 * @params {number} n
 * @return {string}
 */
export const spanOnNthWord = (words, n) => {
  return spanBetweenNthWords(words, n, 1);
}
/**
 * Place a span on the nth word
 * @params {string[]} words
 * @params {number} n
 * @params {number} length
 * @return {string}
 */
export const spanBetweenNthWords = (words, n, length) => {
  const left = words.slice(0, n);
  const wordArray = words.slice(n, n + length);
  const offset = n > 0 ? n + length + 1 : n + length;
  const right = words.slice(offset);

  return `${left.join(" ")} <span class="HasShape">${wordArray.join(" ")}</span> ${right.join(" ")}`;
}

/**
 * Render rich text
 * @param {RichText} arr 
 * @returns 
 */
export const customRenderer = (arr) => {
  return arr.map(
    ({ text, spans }) => {
      if (spans) {
        let spannedText = text;
        spans.forEach(
          ({ start, end, type }) => {
            spannedText = spannedText.split("");
            spannedText.splice(start, 0, `<${type} class="HasShape">`);
            spannedText.splice(end + 1, 0, `</${type}>`);
          }
        )

        return Array.isArray(spannedText) ? spannedText.join("") : spannedText;
      }

      return text;
    }
  )
}