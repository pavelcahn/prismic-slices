import React from "react";

const MOBILE_BREAKPOINT = 769;

/**
 * Hook : switch from true to false when window width is over breakpoint
 * @param {Number} breakpoint Mobile breakpoint (window width)
 */
const useBreakpoint = function (breakpoint = MOBILE_BREAKPOINT) {
  const checkMobile = React.useCallback(
    () => {
      if (typeof window !== "undefined") {
        return window.innerWidth < breakpoint;
      }

      // Suppose we are in mobile if no window is declared
      return true;
    },
    [breakpoint]
  );
  const [isMobile, setMobile] = React.useState(null);

  // Update on resize
  const onResize = React.useCallback(
    () => setMobile(checkMobile),
    [checkMobile]
  );

  React.useEffect(
    () => {
      onResize();
      window.addEventListener('resize', onResize);
      return () => {
        window.removeEventListener('resize', onResize);
      }
    },
    [onResize]
  );

  return isMobile;
}

export default useBreakpoint;