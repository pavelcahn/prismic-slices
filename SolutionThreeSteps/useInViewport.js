import React from "react";
import scrollMonitor from "scrollmonitor";

/**
 * Default viewport setter callback
 */
const defaultViewportChange = (watcher) => {
  return watcher.isInViewport || watcher.isFullyInViewport;
}

/**
 * Check if ref is in viewport
 */
export default function useInViewport(ref, options = {}) {
  const useScroll = options.useScroll || false;
  const setViewport = options.setViewport || defaultViewportChange;
  const [isInViewport, setViewportState] = React.useState(false);
  // Watched element
  const container = ref || React.useRef();
  // Scrollmonitor watcher
  const watcher = React.useRef();

  /**
   * Set monito if container is in viewport 
   */
  const watchScroll = React.useCallback(
    () => setTimeout(setViewportState((setViewport(watcher.current))), 0),
    [setViewportState, setViewport]
  );

  // Watch visibility change on container
  React.useEffect(() => {
    // Only on browser
    if (typeof window !== "undefined") {
      const monitor = scrollMonitor.create(container.current);
      watcher.current = monitor;

      if (useScroll) window.addEventListener("scroll", watchScroll);
      monitor.stateChange(watchScroll);
      watchScroll();

      return () => {
        if (useScroll) window.removeEventListener("scroll", watchScroll);
        monitor.destroy();
        watcher.current = null;
      }
    }
  }, [
    watchScroll,
    useScroll
  ]);

  return [container, isInViewport];
}