import React from 'react';
import Image from 'next/image';
import { imageLoader } from '/prismic-configuration';
/**
 * Wrapper around next/image component
 */
const PrismicImage = (
  {
    url,
    objectFit,
    dimensions = {},
    layout = `responsive`,
    base64 = `data:image/png;base64,`,
    sizes = `100vw`,
    alt = ``,
    ...props
  }
) => {
  if (!url) return null;

  const imgSize = layout !== `fill` ? {
    width: dimensions.width || 0,
    height: dimensions.height || 0,
  } : {};


  return <Image
    {...imgSize}
    src={url}
    alt={alt}
    loader={imageLoader}
    blurDataURL={base64}
    placeholder={`blur`}
    layout={layout}
    sizes={sizes}
    objectFit={objectFit}
    {...props}
  />
}

export default PrismicImage;