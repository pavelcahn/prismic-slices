import React from "react";
import useInViewport from "./useInViewport";

/**
 * Play function
 * @param {Lottie.Animation} animation Lottie animation
 * @returns {function} Pause function
 */
function playAnimation(animation) {
  animation.play();
  return () => animation.pause();
}

/**
 * Play animation on scroll
 * @param {Object} json Path to json animation
 * @param {Object} options Lottie options
 * @param {function} play Play function, should return the pause function
 * @returns {React.ref} Ref to attach to animation container
 */
export default function useAnimation(json, options = {}, play = playAnimation) {
  const [container, isInViewport] = useInViewport();
  // Lottie animation
  const animation = React.useRef();
  // Pause animation function
  const pause = React.useRef();
  // loading set
  const animationIsLoading = React.useRef();

  /**
   * Animation options
   */
  const animParams = React.useCallback(
    (container) => {
      return {
        ...options,
        container,
        renderer: "svg",
        path: json
      }
    },
    [json, options]
  );

  /**
   * Load lottie then load the animation
   */
  const loadAnimation = React.useCallback(
    () => {
      return import('lottie-web')
        .then(({ default: lottie }) => lottie)
        .then((lottie) => lottie.loadAnimation(animParams(container.current)))
        .then((anim) => { animation.current = anim; })
    },
    [json]
  );

  /**
   * Check if container is in viewport and play / pause animation accordingly.
   */
  const watchScroll = React.useCallback(
    () => {
      // Animation is in viewport, it should PLAY
      if (isInViewport) {
        // Load animation on first run
        if (!animation.current) {
          if (!animationIsLoading.current) {
            animationIsLoading.current = true;
            loadAnimation().then(watchScroll);
          }
        } else {

          // Play animation if loaded
          if (animation.current.isLoaded) {
            requestAnimationFrame(() => {
              pause.current = play(animation.current);
            });
          } else {
            // Wait 100ms and before requesting the animation to play
            setTimeout(() => watchScroll(), 1000);
          }
        }
      } else {
        // Animation is not in viewport and should pause
        pause.current && pause.current();
      }
    },
    [
      play,
      loadAnimation,
      isInViewport
    ]
  );

  React.useEffect(watchScroll, [watchScroll]);

  return [container, animation];
}