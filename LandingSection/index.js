// Live here : https://www.supermood.com/en-gb


import React from 'react';
import { RichText } from 'prismic-reactjs';

// Utils
import r from "./renderer";
import { PrismicLink } from "/prismic-configuration";
import PrismicImage from "./prismicImg";
import W from "./W";
import data from "./index.json";

// Css
import style from "./index.module.scss";
const calProgressWithIndex = (progress, index) => {
  const {x,y} = data[index];
  return { x: parseInt(100 * (1-progress) * x) / 100, y: parseInt(100 * (1-progress) * y) / 100};
}

const LandingSection = r()
  .withRender(
    ({ title, description, button, link, items }) => {
      const container = React.useRef();
      const [progress, setProgress] = React.useState(0);

      // Parallax effect
      React.useEffect(
        () => {
          const fn = () => {
            if (container.current) {
              const rect = container.current.getBoundingClientRect();
              // Start parallax when screen enters viewport
              const progress = (rect.y - window.innerHeight) / window.innerHeight;

              window.requestAnimationFrame(() => setProgress(Math.min(1, Math.max(0, -progress))));
            }
          }

          window.addEventListener("scroll", fn);
          return () => window.removeEventListener("scroll", fn);
        },
        []
      );


      return (
        <section 
          ref={container}
          className={`${style.Component} Relative`} 
          data-bg={`BgWhite`}
        >
          
          <W className={` LogoBg--Cream`}/>

          <div className={`wrapper`}>
            <div className={`grid-flex--center`}>
              <div className={`grid-flex__item md6 lg6`}>
                {/* Title */}
                <h2 className={`t3 Criteria mb3 ${style.Title} HasShape`}>
                  {RichText.asText(title)}
                </h2>
                {/* Content */}
                <p className={`mb2`}>
                  {RichText.asText(description)}
                </p>
                <ul className={`${style.Values} Bold mb5`}>
                  {items.map(({value}, key) => <li key={`li-${key}`}>{value}</li>)}
                </ul>

                <span
                  aria-hidden={true}
                  className={style.Placeholder}
                />
                
                {/* CTA */}
                <PrismicLink
                  className={`Btn Btn--Large Btn--Black`}
                  {...link}
                >
                  {button}
                </PrismicLink>
              </div>
              <div className={`grid-flex__item md6 lg6`}>
                <div className={style.Image}>
                  {
                    items.slice(0,3).map(
                      ({image},key) => <Image 
                        image={image} 
                        key={`image-${key}`} 
                        xy={calProgressWithIndex(progress, key)}
                      />
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        </section>
      )
    }
  )

LandingSection.displayName = LandingSection

export default LandingSection;


const Image = ({ image, xy}) => {
  const {x,y} = xy;
  return (
    <div 
      className={`${style.Card} Card`}
      style={
        {
          transform: `translate(${x}%,${y}%)`
        }
      }
    >
      <PrismicImage
        {...image}
        alt={image.alt}
        sizes={`292px`}
      />
    </div>
  )
}